﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Services
{
    public class ChatbotUserService : IChatbotUserService
    {
        private readonly ILogger _logger;
        private readonly AppDbContenxt _AppDbContext;
        public ChatbotUserService(AppDbContenxt appDbContenxt, ILogger<ChatbotUserService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
        }

        public long? Count(DataTableParamModel param)
        {
            return _AppDbContext.ChatbotUser.Count(x => x.IsDeleted == false
                && ((x.ChannelType.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.ChannelUserId.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.FullName.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.Email.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.Address.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.Phone.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)))
                );
        }

        public List<ChatbotUserViewModel> GetChatbotUsers(DataTableParamModel param)
        {
            string[] orderMapping = new string[] { "CreateDt", "CreateDt", "ChannelUserId", "ChannelType", "FullName", "Email", "Phone", "Address","Rating" };
            string orderBy = orderMapping[param.orderCol];
            bool isDesc = true;
            if (param.orderType == "asc") isDesc = false;

            var data_ChatbotUser =
                (
                from _chatbotUser in _AppDbContext.ChatbotUser
                where
                    _chatbotUser.IsDeleted == false
                    && (_chatbotUser.ChannelType.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatbotUser.ChannelUserId.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatbotUser.FullName.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatbotUser.Email.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatbotUser.Phone.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatbotUser.Address.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)) select _chatbotUser
                ).OrderByColumn(orderBy, isDesc).Skip(param.start).Take(param.length).ToList();
            ;
            List<ChatbotUserViewModel> result = new List<ChatbotUserViewModel>();
            if (data_ChatbotUser != null)
            {
                foreach(var item in data_ChatbotUser)
                {
                    result.Add(new ChatbotUserViewModel
                    { 
                        Id = item.Id,
                        ChannelType = item.ChannelType,
                        ChannelUserId = item.ChannelUserId,
                        Address = item.Address,
                        Email = item.Email,
                        FullName = item.FullName,
                        Phone = item.Phone,
                        Rating = item.Rating,
                        CreatedDate = item.CreateDt.ToString("yyyy-MM-dd HH:mm:ss")
                    });
                }
            }
            return result;
        }

        public ChatbotUserViewModel GetChatbotUserDetail(string id)
        {
            var chatbotUser = _AppDbContext.ChatbotUser.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (chatbotUser == null)
                return null;
            ChatbotUserViewModel chatbotUserViewModel = new ChatbotUserViewModel()
            {
                Id = chatbotUser.Id,
                ChannelType = chatbotUser.ChannelType,
                ChannelUserId = chatbotUser.ChannelUserId,
                Address = chatbotUser.Address,
                Email = chatbotUser.Email,
                FullName = chatbotUser.FullName,
                Phone = chatbotUser.Phone,
                Rating = chatbotUser.Rating,
                CreatedDate = chatbotUser.CreateDt.ToString("yyyy-MM-dd HH:mm:ss")
            };
            return chatbotUserViewModel;
        }


    }

    public interface IChatbotUserService
    {
        long? Count(DataTableParamModel param);
        List<ChatbotUserViewModel> GetChatbotUsers(DataTableParamModel param);
        ChatbotUserViewModel GetChatbotUserDetail(string id);
    }
}
