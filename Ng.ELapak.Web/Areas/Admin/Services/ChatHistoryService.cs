﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Services
{
    public class ChatHistoryService : IChatHistoryService
    {
        private readonly ILogger _logger;
        private readonly AppDbContenxt _AppDbContext;
        public ChatHistoryService(AppDbContenxt appDbContenxt, ILogger<ChatHistoryService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
        }

        public long? Count(DataTableParamModel param)
        {
            return _AppDbContext.ChatHistoryFAQ.Count(x => x.IsDeleted == false
                && ((x.MessageContent.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.MessageResponse.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.SessionId.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)))
                );
        }

        public List<ChatHistoryViewModel> GetChatHistory(DataTableParamModel param)
        {
            string[] orderMapping = new string[] { "CreateDt", "MessageContent", "MessageResponse", "Date", "SessionId" };
            string orderBy = orderMapping[param.orderCol];
            bool isDesc = true;
            if (param.orderType == "asc") isDesc = false;

            var data_ChatHistory =
                (
                from _chatHistory in _AppDbContext.ChatHistoryFAQ
                where
                    _chatHistory.IsDeleted == false
                    && (_chatHistory.MessageContent.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatHistory.MessageResponse.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _chatHistory.SessionId.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search))
                select _chatHistory
                ).OrderByColumn(orderBy, isDesc).Skip(param.start).Take(param.length).ToList();
            ;
            List<ChatHistoryViewModel> result = new List<ChatHistoryViewModel>();
            if (data_ChatHistory != null)
            {
                foreach (var item in data_ChatHistory)
                {
                    result.Add(new ChatHistoryViewModel
                    {
                        Id = item.Id,
                        MessageContent = item.MessageContent,
                        MessageResponse = item.MessageResponse,
                        SessionId = item.SessionId,
                        Date = item.Date.ToString("yyyy-MM-dd HH:mm:ss")
                    });
                }
            }
            return result;
        }
    }

    public interface IChatHistoryService
    {
        long? Count(DataTableParamModel param);
        List<ChatHistoryViewModel> GetChatHistory(DataTableParamModel param);
    }
}
