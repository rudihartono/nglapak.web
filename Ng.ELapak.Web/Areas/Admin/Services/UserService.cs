﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Model;
using Ng.ELapak.Web.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Services
{
    public class UserService : IUserService
    {
        private readonly ILogger _logger;
        private readonly AppDbContenxt _AppDbContext;
        public UserService(AppDbContenxt appDbContenxt, ILogger<UserService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
        }

        public long? Count(DataTableParamModel param)
        {
            return _AppDbContext.User.Count(x => x.IsDeleted == false
                && ((x.Username.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.Name.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                || x.Role.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)))
                );
        }

        public List<UserViewModel> GetUsers(DataTableParamModel param)
        {
            string[] orderMapping = new string[] { "CreateDt", "CreateDt", "Username", "Name", "Role" };
            string orderBy = orderMapping[param.orderCol];
            bool isDesc = true;
            if (param.orderType == "asc") isDesc = false;

            var data_User =
                (
                from _user in _AppDbContext.User
                where
                    _user.IsDeleted == false
                    && (_user.Username.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _user.Name.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search)
                    || _user.Role.Contains(String.IsNullOrEmpty(param.search) ? "" : param.search))
                select _user
                ).OrderByColumn(orderBy, isDesc).Skip(param.start).Take(param.length).ToList();
            ;
            List<UserViewModel> result = new List<UserViewModel>();
            if (data_User != null)
            {
                foreach (var item in data_User)
                {
                    result.Add(new UserViewModel
                    {
                        Id = item.Id,
                        Username = item.Username,
                        Name = item.Name,
                        Password = item.Password,
                        Role = item.Role
                    });
                }
            }
            return result;
        }

        public UserViewModel GetUser(string id)
        {
            User user = _AppDbContext.User.Where(x => x.Id == id).FirstOrDefault();
            UserViewModel result = new UserViewModel();
            if (user != null)
            {
                result = new UserViewModel()
                {
                    Id = user.Id,
                    Username = user.Username,
                    Name = user.Name,
                    Password = user.Password,
                    Role = user.Role
                };
            }
            return result;
        }

        public bool AddUser(UserViewModel model)
        {
            User user = _AppDbContext.User.Where(x => x.Id == model.Id).FirstOrDefault();
            bool isUpdate = false;
            if (user == null)
            {
                user = new User()
                {
                    Name = model.Name,
                    Username = model.Username,
                    Password = model.Password,
                    Role = model.Role,
                    CreateDt = DateTime.Now
                };
            }
            else
            {
                isUpdate = true;
                user.Name = model.Name;
                user.Password = model.Password;
                user.Role = model.Role;
                user.UpdatedDt = DateTime.Now;
            }

            try
            {
                if (isUpdate)
                    _AppDbContext.User.Update(user);
                else
                    _AppDbContext.User.Add(user);
                _AppDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public bool DelUser(string id)
        {
            User user = _AppDbContext.User.Where(x => x.Id == id).FirstOrDefault();
            if (user == null)
            {
                return false;
            }
            else
            {
                user.IsDeleted = true;
                try
                {
                    _AppDbContext.User.Update(user);
                    _AppDbContext.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }
    }

    public interface IUserService
    {
        long? Count(DataTableParamModel param);
        List<UserViewModel> GetUsers(DataTableParamModel param);
        UserViewModel GetUser(string id);
        bool AddUser(UserViewModel model);
        bool DelUser(string id);
    }
}
