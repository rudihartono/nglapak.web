﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly ILogger _logger;
        private readonly AppDbContenxt _AppDbContext;
        public DashboardService(AppDbContenxt appDbContenxt, ILogger<DashboardService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
        }
        public double ChatbotUserCount()
        {
            var ret = _AppDbContext.ChatbotUser.ToList();
            if (ret == null)
                return 0;
            return ret.Count(x => x.IsDeleted == false
                );
        }

        public double TotalChat()
        {
            var ret = _AppDbContext.TotalChatByDate.ToList();
            if(ret == null)
                return 0;
            return ret.Sum(x => x.Total);
        }

        public double AverageRating()
        {
            var ret = _AppDbContext.ChatbotUser.ToList();
            if (ret == null || ret.Count == 0)
                return 0;
            return ret.Where(x => x.IsDeleted == false
                ).Select(x => x.Rating).Average();
        }

        public List<ChatbotUserViewModel> GetNewChatbotUsers()
        {
            var data_ChatbotUser =
                (
                from _chatbotUser in _AppDbContext.ChatbotUser
                where
                    _chatbotUser.IsDeleted == false
                select _chatbotUser
                ).OrderByColumn("CreateDt", false).Take(5);
            ;
            List<ChatbotUserViewModel> result = new List<ChatbotUserViewModel>();
            if (data_ChatbotUser != null)
            {
                foreach (var item in data_ChatbotUser)
                {
                    result.Add(new ChatbotUserViewModel
                    {
                        Id = item.Id,
                        ChannelType = item.ChannelType,
                        ChannelUserId = item.ChannelUserId,
                        Address = item.Address,
                        Email = item.Email,
                        FullName = item.FullName,
                        Phone = item.Phone,
                        Rating = item.Rating,
                        CreatedDate = item.CreateDt.ToString("yyyy-MM-dd HH:mm:ss")
                    });
                }
            }
            return result;
        }

        public List<MonthlyGrowthUser> GetMonthlyGrowthUser()
        {
            var chatBotUser = _AppDbContext.ChatbotUser.Where(x => x.IsDeleted == false);
            List<MonthlyGrowthUser> listUser = new List<MonthlyGrowthUser>();
            if (chatBotUser != null)
            {
                MonthlyGrowthUser user = null;
                for (int i = 1; i <= 12; i++)
                {
                    user = new MonthlyGrowthUser();
                    user.Month = ((MonthName)i - 1).ToString();
                    user.Total = chatBotUser.Where(x => x.CreateDt.Month == i).Count();
                    listUser.Add(user);
                }
            }
            return listUser;
        }

        public List<MonthlyGrowthChat> GetMonthlyGrowthChat()
        {
            var chatHistory = _AppDbContext.TotalChatByDate.Where(x => x.IsDeleted == false).ToList();
            List<MonthlyGrowthChat> listChat = new List<MonthlyGrowthChat>();
            if (chatHistory != null)
            {
                MonthlyGrowthChat chat = null;
                for (int i = 1; i <= 12; i++)
                {
                    chat = new MonthlyGrowthChat();
                    chat.Month = ((MonthName)i - 1).ToString();
                    chat.Total = chatHistory.Where(x => x.Date.Month == i).Sum(x => x.Total);
                    listChat.Add(chat);
                }
            }
            return listChat;
        }

    }
    public interface IDashboardService
    {
        double TotalChat();
        double ChatbotUserCount();
        double AverageRating();
        List<ChatbotUserViewModel> GetNewChatbotUsers();
        List<MonthlyGrowthChat> GetMonthlyGrowthChat();
        List<MonthlyGrowthUser> GetMonthlyGrowthUser();
    }
}
