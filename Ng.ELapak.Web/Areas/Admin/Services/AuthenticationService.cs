﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Services
{
    public class AuthenticationService
    {
        private int _expired;
        private bool _useCustomUserIdentity;
        private string _userIdentityName;
        private string _identityType;
        private readonly ILogger _logger;

        public AuthenticationService()
        {
            _useCustomUserIdentity = false;
            _expired = 14;
        }

        public AuthenticationService(IOptions<AuthenticationSetting> options,
            ILogger<AuthenticationService> logger)
        {
            _logger = logger;
            if (options.Value == null)
            {
                _useCustomUserIdentity = false;
                _expired = 14;
            }
            else
                SetUserClaimIdentity(options.Value);
        }
        public AuthenticationService(bool userCustomUserIdentity, string userIdentityName, string identityType)
        {
            _useCustomUserIdentity = userCustomUserIdentity;
            _userIdentityName = userIdentityName;
            _identityType = identityType;
        }

        private void SetUserClaimIdentity(AuthenticationSetting value)
        {
            _expired = value.Expired;
            _useCustomUserIdentity = value.UseCustomUserIdentity;
            _userIdentityName = value.UserIdentityName;
            _identityType = value.IdentityType;
        }


        public virtual async void Set(object obj, string role, bool isRemember, string key, Microsoft.AspNetCore.Http.HttpContext context)
        {
            string identifier = key + "|" + DateTime.Now.ToString("yyyyMMddHHmmssffff");

            string strJsonUser = JsonConvert.SerializeObject(obj);

            var ident = GetClaimIdentity(strJsonUser, role, identifier);

            await context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(ident),
                    new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTime.UtcNow.AddDays(_expired)
                    });
        }

        protected virtual ClaimsIdentity GetClaimIdentity(string strObj, string role, string identifier)
        {
            if (_useCustomUserIdentity)
            {
                var userClaimIdentity = new UserClaimIdentity(_userIdentityName, _identityType);

                return new ClaimsIdentity(userClaimIdentity,
                   new[]
                   {
                    new Claim(ClaimTypes.NameIdentifier, identifier),
                    new Claim(ClaimTypes.UserData, strObj),
                    new Claim(ClaimTypes.Authentication, identifier),
                    new Claim(ClaimTypes.Role, role),
                   });
            }

            return new ClaimsIdentity(
                   new[]
                   {
                    new Claim(ClaimTypes.NameIdentifier, identifier),
                    new Claim(ClaimTypes.UserData, strObj),
                    new Claim(ClaimTypes.Authentication, identifier),
                    new Claim(ClaimTypes.Role, role),
                   }, CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public T Get<T>(Microsoft.AspNetCore.Http.HttpContext context) where T : class
        {
            T userAuth = null;

            if (!(context.User != null &&
                context.User.Identity != null /*&&
                httpContext.User.Identity.IsAuthenticated*/))
            {
                return null;
            }

            try
            {
                var contractResolver = new DefaultContractResolver();
                contractResolver.DefaultMembersSearchFlags |= System.Reflection.BindingFlags.NonPublic;
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = contractResolver
                };

                var userData = ((ClaimsIdentity)context.User.Identity).FindFirst(ClaimTypes.UserData);
                userAuth = JsonConvert.DeserializeObject<T>(userData.Value, settings);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }

            return userAuth;
        }

        public async Task LogOut(Microsoft.AspNetCore.Http.HttpContext httpContext)
        {
            await httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }

    public class UserClaimIdentity : IIdentity
    {
        private string _Name { get; set; }
        private string _AuthenticationType { get; set; }

        public UserClaimIdentity(string name, string authName)
        {
            _Name = name;
            _AuthenticationType = authName;
        }
        public string AuthenticationType => _AuthenticationType;

        public bool IsAuthenticated => true;

        public string Name => _Name;
    }

}
