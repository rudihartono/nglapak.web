﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Models
{
    public class UserAuthenticated
    {
        public UserAuthenticated()
        {

        }

        public UserAuthenticated(string id, string fullname, string roleName)
        {
            Id = id;
            Fullname = fullname;
            RoleName = roleName;
        }

        public string Id { get; private set; }
        public string Fullname { get; private set; }
        public string RoleName { get; set; }
    }
}
