﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Models
{
    public class AuthenticationSetting
    {
        public int Expired { get; set; }
        public bool UseCustomUserIdentity { get; set; }
        public string UserIdentityName { get; set; }
        public string IdentityType { get; set; }
    }
}
