﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Models
{
    public class ChatbotUserViewModel
    {
        public string Id { get; set; }
        public string ChannelUserId { get; set; }
        public string ChannelType { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string CreatedDate { get; set; }
        public int Rating { get; set; }
    }
}
