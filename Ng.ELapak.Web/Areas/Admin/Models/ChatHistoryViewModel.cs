﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Models
{
    public class ChatHistoryViewModel
    {
        public string Id { get; set; }
        public string MessageContent { get; set; }
        public string MessageResponse { get; set; }
        public string SessionId { get; set; }
        public string Date { get; set; }
    }
}
