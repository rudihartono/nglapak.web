﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Models
{
    public class DashboardViewModel
    {
        public List<MonthlyGrowthUser> ListMonthlyGrowthUser { get; set; }
        public List<MonthlyGrowthChat> ListMonthlyGrowthChat { get; set; }
    }
    public class MonthlyGrowthUser 
    {
        public string Month { get; set; }
        public long Total { get; set; }
    }

    public class MonthlyGrowthChat
    {
        public string Month { get; set; }
        public long Total { get; set; }
    }

    public enum MonthName
    {
        Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
    }
}
