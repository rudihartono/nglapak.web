﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Models
{
    public class DataTableParamModel
    {
        public string target { get; set; }
        public string search { get; set; }
        public int length { get; set; }
        public int start { get; set; }
        public int orderCol { get; set; }
        public string orderType { get; set; }
    }
}
