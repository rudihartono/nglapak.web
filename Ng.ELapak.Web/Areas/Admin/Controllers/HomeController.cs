﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Areas.Admin.Services;
using Ng.ELapak.Web.Domain.Db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : BaseController
    {
        private readonly ILogger _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            if (_userAuth == null)
                return RedirectToAction("Index", "Authentication");
            else
                return RedirectToAction("Index", "Dashboard");
        }
    }
}