﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Areas.Admin.Services;
using Ng.ELapak.Web.Domain.Db;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class UserController : BaseController
    {

        private readonly IUserService _userbotService;

        public UserController(AppDbContenxt appDbContenxt, ILogger<UserService> logger)
        {
            _userbotService = new UserService(appDbContenxt, logger);
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Detail(string id)
        {
            UserViewModel user = _userbotService.GetUser(id);
            return View(user);
        }

        public IActionResult GetDataUsers(DataTableParamModel param)
        {
            string headers = Request.Headers.ToString();
            return Json(new
            {
                iTotalRecords = _userbotService.Count(param),
                iTotalDisplayRecords = _userbotService.Count(param),
                aaData = _userbotService.GetUsers(param)
            });
        }

        public IActionResult Edit(string id)
        {
            UserViewModel user = _userbotService.GetUser(id);
            return View(user);
        }

        public IActionResult Add()
        {
            UserViewModel user = new UserViewModel();
            return View("Edit", user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(UserViewModel model)
        {
            if(model.Password != model.ConfirmPassword)
            {
                TempData["AlertError"] = "Wrong Password Confirmation, please check again";
                return View("Edit", model);
            }
            if (!ModelState.IsValid)
            {
                TempData["AlertError"] = string.Join(System.Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                return View(model);
            }
            else
            {
                if (_userbotService.AddUser(model))
                {
                    TempData["AlertSuccess"] = "Edit User Success";
                    return Redirect("/Admin/User");
                }
                else
                {
                    TempData["AlertError"] = "Edit User Failed";
                    return Redirect("/Admin/User");
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(UserViewModel model)
        {
            
            if (model.Password != model.ConfirmPassword)
            {
                TempData["AlertError"] = "Wrong Password Confirmation, please check again";
                return View("Edit", model);
            }
            if (!ModelState.IsValid)
            {
                TempData["AlertError"] = string.Join(System.Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                return View(model);
            }
            else
            {
                if (_userbotService.AddUser(model))
                {
                    TempData["AlertSuccess"] = "Add User Success";
                    return Redirect("/Admin/User");
                }
                else
                {
                    TempData["AlertError"] = "Add User Failed";
                    return Redirect("/Admin/User");
                }
            }
        }

        public IActionResult Delete(string id)
        {
            if (_userbotService.DelUser(id))
            {
                TempData["AlertSuccess"] = "Delete User Success";
                //return RedirectToAction(nameof(Index));
                //return Redirect("/Admin/User");
                return Redirect("/Admin/User");
            }
            TempData["AlertError"] = "Delete User Failed";
            //return RedirectToAction(nameof(Index));
            return Redirect("/Admin/User");

        }
    }
}