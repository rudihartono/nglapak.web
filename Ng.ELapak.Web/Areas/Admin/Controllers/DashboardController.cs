﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Areas.Admin.Services;
using Ng.ELapak.Web.Domain.Db;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class DashboardController : BaseController
    {
        private readonly IDashboardService _dashboardService;
        public DashboardController(AppDbContenxt appDbContenxt, ILogger<DashboardService> logger)
        {
            _dashboardService = new DashboardService(appDbContenxt, logger);
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Dashboard";
            ViewBag.ActiveMenu = new string[] { "dashboard-index" };
            ViewBag.ChatbotUserCount = _dashboardService.ChatbotUserCount();
            ViewBag.Rating = _dashboardService.AverageRating().ToString("0.00");
            ViewBag.NewUser = _dashboardService.GetNewChatbotUsers();
            ViewBag.TotalChat = _dashboardService.TotalChat();
            DashboardViewModel model = new DashboardViewModel();
            model.ListMonthlyGrowthUser = _dashboardService.GetMonthlyGrowthUser();
            model.ListMonthlyGrowthChat = _dashboardService.GetMonthlyGrowthChat();
            return View(model);
        }
    }
}