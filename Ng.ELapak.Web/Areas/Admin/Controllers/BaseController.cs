﻿using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Areas.Admin.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected UserAuthenticated _userAuth;
        protected readonly AuthenticationService _authenticationService;

        public BaseController()
        {
            _authenticationService = new AuthenticationService();
        }

        public BaseController(IOptions<AuthenticationSetting> options, AuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            GetAuthorizationUser();
            base.OnActionExecuting(context);
        }

        private void GetAuthorizationUser()
        {
            if(User != null && User.Claims.Any())
                _userAuth = _authenticationService.Get<UserAuthenticated>(HttpContext);
        }

        protected async virtual void Logout()
        {
            await _authenticationService.LogOut(this.HttpContext);
        }
    }
}
