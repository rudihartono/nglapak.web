﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Areas.Admin.Services;
using Ng.ELapak.Web.Domain.Db;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class ChatbotUserController : BaseController
    {
        private readonly IChatbotUserService _chatUserbotService;
        public ChatbotUserController(AppDbContenxt appDbContenxt, ILogger<ChatbotUserService> logger)
        {
            _chatUserbotService = new ChatbotUserService(appDbContenxt, logger);
        }
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult GetDataChatbotUsers(DataTableParamModel param)
        {
            string headers = Request.Headers.ToString();
            return Json(new
            {
                iTotalRecords = _chatUserbotService.Count(param),
                iTotalDisplayRecords = _chatUserbotService.Count(param),
                aaData = _chatUserbotService.GetChatbotUsers(param)
            });
        }

        public IActionResult Detail(string id)
        {
            var chatbotUser = _chatUserbotService.GetChatbotUserDetail(id);
            if (chatbotUser == null)
            {
                return RedirectToAction("Index");
            }
            return View(chatbotUser);
        }
    }
}