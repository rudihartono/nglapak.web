﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class ChatHistoryController : BaseController
    {
        private readonly Services.IChatHistoryService _chatHistoryService;

        public ChatHistoryController(AppDbContenxt appDbContenxt, ILogger<Services.ChatHistoryService> logger)
        {
            _chatHistoryService = new Services.ChatHistoryService(appDbContenxt, logger);
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetDataChatHistory(DataTableParamModel param)
        {
            string headers = Request.Headers.ToString();
            return Json(new
            {
                iTotalRecords = _chatHistoryService.Count(param),
                iTotalDisplayRecords = _chatHistoryService.Count(param),
                aaData = _chatHistoryService.GetChatHistory(param)
            });
        }
    }
}