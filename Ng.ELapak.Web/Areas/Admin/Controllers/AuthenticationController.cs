﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Domain.Db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using System.Text;

namespace Ng.ELapak.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AuthenticationController : BaseController
    {
        private const string DEFAULT_USERNAME = "superadmin";
        private const string PASSWORD = "administrator";
        private const string ROLE = "ADMIN";

        private readonly ILogger _logger;
        private readonly AppDbContenxt _AppDbContext;
        public AuthenticationController(AppDbContenxt appDbContenxt,
            ILogger<AuthenticationController> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([Bind("Username,Password")] LoginReqViewModel viewModel)
        {

            if(!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = GetErrorFromModelState(ModelState);
                return View(viewModel);
            }

            Domain.Model.User userAuthenticated = null;

            _authenticationService.Set(new UserAuthenticated(userAuthenticated.Id, userAuthenticated.Name, userAuthenticated.Role),
                role: userAuthenticated.Role,
                false,
                "Ng.ELapak.WEB",
                HttpContext);

            if (viewModel.Username == DEFAULT_USERNAME)
            {
                if(viewModel.Username == DEFAULT_USERNAME &&  viewModel.Password == PASSWORD)
                {
                    userAuthenticated = new Domain.Model.User()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Username = DEFAULT_USERNAME,
                        IsDeleted = false,
                        Name = DEFAULT_USERNAME,
                        Role = DEFAULT_USERNAME,
                        CreateDt = DateTime.UtcNow,
                        Type = Domain.Model.UserType.ADMIN
                    };
                }
            }else
            {
                await Task.Run(() =>
                {
                    userAuthenticated = _AppDbContext.User.Where(x => x.Username == viewModel.Username &&
                x.Password == viewModel.Password && !x.IsDeleted).FirstOrDefault();
                });
            }

            return RedirectToAction("Index", "Dashboard");
        }

        public async Task<IActionResult> Logout()
        {
            await _authenticationService.LogOut(this.HttpContext);

            return RedirectToAction("Index", "Home");
        }

        private dynamic GetErrorFromModelState(ModelStateDictionary modelState)
        {
            if (modelState.ErrorCount == 0)
                return string.Empty;

            var errorResult = modelState.Select(x => x.Value.Errors.Select(y => y.ErrorMessage)).ToList();

            if (!errorResult.Any())
                return string.Empty;

            return string.Join("\n", errorResult.Select(x=> x.FirstOrDefault()));
        }
    }
}
/*
if (userAuthenticated == null)
{
    ViewBag.ErrorMessage = "Invalid username or password";
    return View(viewModel);
}
*/
