﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ng.ELapak.Web.Domain.Db;

namespace Ng.ELapak.Web.Areas.ecommerce.Controllers
{
    [Area("ecommerce")]
    public class ProductController : Controller
    {
        private readonly AppDbContenxt _context;
        public ProductController(AppDbContenxt context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] string code)
        {
            var product = await _context.Product.Where(x => x.Code == code).FirstOrDefaultAsync();

            return View(product);
        }
    }
}