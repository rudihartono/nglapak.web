﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ng.ELapak.Web.Domain.Db;

namespace Ng.ELapak.Web.Areas.ecommerce.Controllers
{
    [Area("ecommerce")]
    public class HomeController : Controller
    {
        private readonly AppDbContenxt _context;

        public HomeController(AppDbContenxt dbContenxt)
        {
            _context = dbContenxt;
        }
        public async Task<IActionResult> Index()
        {
            var ListProduct = await _context.Product.ToListAsync();
            return View(ListProduct);
        }
    }
}