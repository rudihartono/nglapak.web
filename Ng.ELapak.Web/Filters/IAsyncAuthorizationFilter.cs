﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Filters
{
    public interface IAsyncAuthorizationFilter : IFilterMetadata
    {
        Task OnAuthorizationAsync(AuthorizationFilterContext context);
    }
}
