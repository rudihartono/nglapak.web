﻿using Ng.ELapak.Web.Extensions;
using Ng.ELapak.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Filters
{
    public class AuthorizationApiFilter : Attribute, IActionFilter
    {
        private ResponseMessage _responseMessage;
        public AuthorizationApiFilter()
        {
            _responseMessage = new ResponseMessage();
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            //string username = "merckchatbot2019";
            //string password = "qwerty123";
            string keyval = "123456";

            bool isError = false;

            try
            {
                StringValues headerVal;
                context.HttpContext.Request.Headers.TryGetValue("Authorization", out headerVal);

                //IEnumerable<string> headerValues = actionContext.HttpContext.Request.Headers.
                //var c = actionContext.HttpContext.Request.Headers.TryGetValue.AuthenticationSchemes("Authorization").FirstOrDefault();
                //string headerVal = actionContext.HttpContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
                string keyHeader = headerVal.FirstOrDefault().Substring("Basic ".Length);

                if (!keyval.Equals(keyHeader))
                {
                    isError = true;
                }
            }
            catch (Exception)
            {
                isError = true;
            }

            if (isError)
            {
                //context.Result = new 
                return;
            }
        }

        //public void OnAuthorization(AuthorizationFilterContext actionContext)
        //{
        //    //string username = "merckchatbot2019";
        //    //string password = "qwerty123";
        //    string keyval = "123456";

        //    bool isError = false;

        //    try
        //    {
        //        StringValues headerVal;
        //        actionContext.HttpContext.Request.Headers.TryGetValue("Authorization", out headerVal);

        //        //IEnumerable<string> headerValues = actionContext.HttpContext.Request.Headers.
        //        //var c = actionContext.HttpContext.Request.Headers.TryGetValue.AuthenticationSchemes("Authorization").FirstOrDefault();
        //        //string headerVal = actionContext.HttpContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
        //        string keyHeader = headerVal.FirstOrDefault().Substring("Basic ".Length);

        //        if (!keyval.Equals(keyHeader))
        //        {
        //            isError = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        isError = true;
        //    }

        //    if (isError)
        //    {
        //        actionContext.Result = new UnauthorizedResult();
        //        return;
        //    }
        //}
    }
}
