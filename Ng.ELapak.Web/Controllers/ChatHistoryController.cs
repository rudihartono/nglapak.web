﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Services;
using Ng.ELapak.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Ng.ELapak.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatHistoryController : ControllerBase
    {
        private readonly ICronChatHistoryService _chatHistoryService;
        public ChatHistoryController(AppDbContenxt appDbContenxt, ILogger<CronChatHistoryService> logger, IOptions<KataConfigurationSetting> setting)
        {
            _chatHistoryService = new CronChatHistoryService(appDbContenxt, logger, setting);
        }

        [HttpPost("updatechathistory")]
        public async Task<IActionResult> UpdateChatHistory(UpdateChatHistoryModel model)
        {
            DateTime StartDate = DateTime.Parse(model.StartDate);
            DateTime EndDate = DateTime.Parse(model.EndtDate);
            int DayInterval = 1;

            List<DateTime> dateList = new List<DateTime>();
            while (StartDate.AddDays(DayInterval) <= EndDate)
            {
                StartDate = StartDate.AddDays(DayInterval);
                await _chatHistoryService.UpdateChatHistoryByPeriode(StartDate);
            }
            
            return Ok();
        }
    }
}