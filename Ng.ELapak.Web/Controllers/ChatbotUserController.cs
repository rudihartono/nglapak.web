﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Services;
using Ng.ELapak.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ng.ELapak.Web.Models;
using Ng.ELapak.Web.Domain.Model;
using Ng.ELapak.Web.Extensions;
using Microsoft.Extensions.Logging;

namespace Ng.ELapak.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatbotUserController : ControllerBase
    {
        private readonly IChatbotUserService _chatUserbotService;
        private readonly ResponseMessage _responseMessage;
        public ChatbotUserController(AppDbContenxt appDbContenxt, ILogger<ChatbotUserService> logger)
        {
            _chatUserbotService = new ChatbotUserService(appDbContenxt, logger);
            _responseMessage = new ResponseMessage();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("checkstatus")]
        public async Task<IActionResult> CheckStatus(ApiBaseModel model)
        {
            
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _chatUserbotService.CheckStatus(model));
            }
            return Ok(_responseMessage);
        }

        [HttpPost("savechatbotuser")]
        public async Task<IActionResult> SaveChatbotUser(ChatbotUserApiModel model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _chatUserbotService.SaveChatbotUser(model));
            }
            return Ok(_responseMessage);
        }

        [HttpPost("saverating")]
        public async Task<IActionResult> SaveRating(RatingUserApiModel model)
        {
            if (!ModelState.IsValid)
            {
                //_responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                //    .SelectMany(v => v.Errors)
                //    .Select(e => e.ErrorMessage)));
                _responseMessage.SetCustomCode(408, string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _chatUserbotService.SaveRating(model));
            }
            return Ok(_responseMessage);
        }

        [HttpPost("handover")]
        public async Task<IActionResult> HandOver(HandOverApiModel model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetCustomCode(408, string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _chatUserbotService.HandOver(model));
            }
            return Ok(_responseMessage);
        }
    }
}