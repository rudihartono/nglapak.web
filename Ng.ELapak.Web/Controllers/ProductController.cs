﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Services;
using Ng.ELapak.Web.Extensions;
using Ng.ELapak.Web.Models;

namespace Ng.ELapak.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        private readonly IProductService _productService;
        private readonly ResponseMessage _responseMessage;
        public ProductController(AppDbContenxt appDbContenxt, ILogger<ProductService> logger)
        {
            _productService = new ProductService(appDbContenxt, logger);
            _responseMessage = new ResponseMessage();
        }

        [HttpPost("detail")]
        public async Task<IActionResult> Detail(ProductApi model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _productService.Detail(model));
            }
            return Ok(_responseMessage);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search(ProductSearchApi model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _productService.Search(model));
            }
            return Ok(_responseMessage);
        }

        [HttpPost("bid")]
        public async Task<IActionResult> Bid(BidProductApi model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _productService.Bid(model));
            }
            return Ok(_responseMessage);
        }

        [HttpPost("purchase")]
        public async Task<IActionResult> Purchase(PurchaseApi model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _productService.Purchase(model));
            }
            return Ok(_responseMessage);
        }
    }
}