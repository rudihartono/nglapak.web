﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Services;
using Ng.ELapak.Web.Extensions;
using Ng.ELapak.Web.Models;

namespace Ng.ELapak.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;
        private readonly ResponseMessage _responseMessage;
        public MessageController(AppDbContenxt appDbContenxt, ILogger<MessageService> logger)
        {
            _messageService = new MessageService(appDbContenxt, logger);
            _responseMessage = new ResponseMessage();
        }

        [HttpPost("postmessage")]
        public async Task<IActionResult> PostMessage(PostMessage model)
        {
            if (!ModelState.IsValid)
            {
                _responseMessage.SetBadRequest(message: string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)));
            }
            else
            {
                return Ok(await _messageService.PostMessage(model));
            }
            return Ok(_responseMessage);
        }
    }
}