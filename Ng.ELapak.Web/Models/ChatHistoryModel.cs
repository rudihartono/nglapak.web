﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Models
{
    public class ChatHistoryModel
    {
        public string status { get; set; }
        public List<ConversationChat> result { get; set; }
    }

    public class UpdateChatHistoryModel
    {
        public string StartDate { get; set; }
        public string EndtDate { get; set; }
    }

    public class ConversationChat
    {
        public string sessionid { get; set; }
        public string messages { get; set; }
        public string responses { get; set; }
        public long created_at { get; set; }
    }

    public class Source
    {
        public string userId { get; set; }
        public string type { get; set; }
    }

    public class Payload
    {
        public string type { get; set; }
        public string replyToken { get; set; }
        public Source source { get; set; }
        public long timestamp { get; set; }
    }

    public class Metadata
    {
        public string channelType { get; set; }
        public string lineSenderId { get; set; }
        public string lineSenderType { get; set; }
        public string lineAccessToken { get; set; }
    }

    public class Attributes
    {
    }

    public class messages
    {
        public string type { get; set; }
        public Payload payload { get; set; }
        public Metadata metadata { get; set; }
        public string id { get; set; }
        public string intent { get; set; }
        public Attributes attributes { get; set; }
        public string content { get; set; }
    }

    public class responses
    {
        public string type { get; set; }
        public string content { get; set; }
        public string action { get; set; }
        public string id { get; set; }
        public string refId { get; set; }
        public string flow { get; set; }
        public string intent { get; set; }
    }

    public class SessionId
    {
        public string sessionid { get; set; }
        public long created_at { get; set; }
    }

    public class KataReturnModel
    {
        public int expire { get; set; }
        public bool isOtp { get; set; }
        public string label { get; set; }
        public string roleId { get; set; }
        public string type { get; set; }
        public string userId { get; set; }
        public string id { get; set; }
    }
}
