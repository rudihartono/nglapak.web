﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Models
{
    public class ResponseMessage
    {
        public const string HeaderNext = "location";
        public const string NewToken = "X-Append-Token";

        [JsonIgnore]
        public Dictionary<string, string> AdditionalHeader { get; set; }

        [JsonProperty(PropertyName = "alert")]
        public ResponseAlertMessage Alerts { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        public ResponseMessage()
        {
            AdditionalHeader = new Dictionary<string, string>();
            Alerts = new ResponseAlertMessage();
            Data = null;
        }

        public void SetDataInternal(object data)
        {
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public void Reset()
        {
            Data = null;
        }
    }
}
