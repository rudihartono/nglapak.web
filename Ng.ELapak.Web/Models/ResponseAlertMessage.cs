﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Models
{
    public class ResponseAlertMessage
    {
        public ResponseAlertMessage()
        {
            this.Code = (int)HttpStatusCode.OK;
            this.Message = "Ok";
            this.InnerMessage = null;
        }

        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "inner_message")]
        public string InnerMessage { get; set; }

        /// <summary>
        /// Set code to 200
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetOK(string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = (int)HttpStatusCode.OK;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }

        /// <summary>
        /// Set code to 401
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetUnauthorized(string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = (int)HttpStatusCode.Unauthorized;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }

        /// <summary>
        /// Set code to 404
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetNotFound(string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = (int)HttpStatusCode.NotFound;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }

        /// <summary>
        /// Set code to 400
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetBadRequest(string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = (int)HttpStatusCode.BadRequest;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }

        /// <summary>
        /// Set code to 403
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetForbidden(string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = (int)HttpStatusCode.Forbidden;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }

        /// <summary>
        /// Set code to 500
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetInternalServerError(string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = (int)HttpStatusCode.InternalServerError;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }

        /// <summary>
        /// Set code to @param
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="innerMessage"></param>
        public void SetCustomCode(int code, string message, string innerMessage = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            this.Code = code;
            this.Message = message;

            if (!string.IsNullOrWhiteSpace(innerMessage))
            {
                this.InnerMessage = innerMessage;
            }
        }
    }
}
