﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Models
{
    public class KataConfigurationSetting
    {
        public string ProjectID { get; set; }
        public string EnvironmentID { get; set; }
        public string KataUser { get; set; }
        public string KataPass { get; set; }
        public string KataUrl { get; set; }
        public string TeamID { get; set; }

    }
}
