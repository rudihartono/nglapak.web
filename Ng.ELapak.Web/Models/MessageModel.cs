﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Models
{
    
    public class MessageSendModel
    {
        public string environmentName { get; set; }
        public object session { get; set; }
        public Message message { get; set; }
    }

    public class Session
    {
        public string id { get; set; }
        public States states { get; set; }
        public Contexes contexes { get; set; }
        public object[] history { get; set; }
        public object current { get; set; }
        public Meta meta { get; set; }
        public long timestamp { get; set; }
        public Data data { get; set; }
    }

    public class Meta
    {
        public string lastFlow { get; set; }
        public string lastState { get; set; }
        public bool end { get; set; }
    }

    public class Data
    {
        public int Regist { get;set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }


    public class MessageResponseModel
    {
        public Message[] messages { get; set; }
        public Respons[] responses { get; set; }
        public long processedAt { get; set; }
        public Session session { get; set; }
        public int duration { get; set; }
        public string converseId { get; set; }
        public long start { get; set; }
        public long end { get; set; }
    }

    public class States
    {
        public string main { get; set; }
    }

    public class Contexes
    {
        public Main main { get; set; }
    }

    public class Main
    {
    }

    public class Message
    {
        public string type { get; set; }
        public string content { get; set; }
        public string id { get; set; }
        public string intent { get; set; }
        public Attributes attributes { get; set; }
        public Payload payload { get; set; }
    }


    public class Respons
    {
        public string type { get; set; }
        public string content { get; set; }
        public string action { get; set; }
        public string id { get; set; }
        public string refId { get; set; }
        public string flow { get; set; }
        public string intent { get; set; }
    }

    public class PostMessage
    {
        public string UserId { get; set; }
        public string Message { get; set; }
    }


}
