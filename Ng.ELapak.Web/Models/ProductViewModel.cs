﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Models
{
    public class ProductViewModel
    {
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }
    }

    public class ProductApi
    {
        public string ProductCode { get; set; }
    }

    public class BidProductApi
    {
        public string ProductCode { get; set; }
        public double BidPrice { get; set; }
    }

    public class PurchaseApi
    {
        public string ProductCode { get; set; }
        public double BidPrice { get; set; }
        public int Qty { get; set; }
        public string UserId { get; set; }
    }

    public class ProductSearchApi
    {
        [Required]
        public string Keyword { get; set; }

        [Required]
        [Range(1, Int16.MaxValue)]
        public int Page { get; set; }

        [Range(1, Int16.MaxValue)]
        public int Limit { get; set; }
    }
}
