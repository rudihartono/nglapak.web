var Dashboard = function() {

    var initChartSample1 = function (monthlyUser) {
        var chart = AmCharts.makeChart("chart_1", {
          "type": "serial",
          "theme": "light",
          "pathToImages": "assets/plugins/amcharts/amcharts/images/",
          "autoMargins": false,
          "marginLeft": 50,
          "marginRight": 0,
          "marginTop": 10,
          "marginBottom": 26,
          "valueAxes": [{
            "id": "v1",
            "stackType": "none",
            "title": "",
            "position": "left",
            "autoGridCount": false,
            "labelFunction": function(value) {
              return Math.round(value);
            }
          }],
          "graphs": [{
            "id": "graph_line1",
            "valueAxis": "v1",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 8,
            "hideBulletsCount": 50,
            "lineThickness": 3,
            "lineColor": "#3C599C",
            "type": "smoothedLine",
            "title": "Total User",
            "useLineColorForBulletBorder": true,
            "valueField": "total",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
          }],
          "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 0,
            "valueLineAlpha": 0.2
          },
          "categoryField": "month",
          "categoryAxis": {
            "dashLength": 1,
            "minorGridEnabled": true
          },
          "legend": {
            "useGraphSettings": true,
            "position": "top",
            "autoMargins": false,
            "marginTop": 0,
            "align": "center"
          },
          "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0.2
          },
          "dataProvider": monthlyUser
        });
    }
    var initChartSample2 = function (monthlyChat) {
        var chart = AmCharts.makeChart("chart_2", {
            "type": "serial",
            "theme": "light",
            "pathToImages": "assets/plugins/amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 50,
            "marginRight": 0,
            "marginTop": 10,
            "marginBottom": 26,
            "valueAxes": [{
                "id": "v1",
                "stackType": "none",
                "title": "",
                "position": "left",
                "autoGridCount": false,
                "labelFunction": function (value) {
                    return Math.round(value);
                }
            }],
            "graphs": [{
                "id": "graph_line1",
                "valueAxis": "v1",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 8,
                "hideBulletsCount": 50,
                "lineThickness": 3,
                "lineColor": "#50AE54",
                "type": "smoothedLine",
                "title": "Total Chat",
                "useLineColorForBulletBorder": true,
                "valueField": "total",
                "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
            }],
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.2
            },
            "categoryField": "month",
            "categoryAxis": {
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "legend": {
                "useGraphSettings": true,
                "position": "top",
                "autoMargins": false,
                "marginTop": 0,
                "align": "center"
            },
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0.2
            },
            "dataProvider": monthlyChat
        });
    }

    return {
        init: function (monthlyUser, monthlyChat) {
            initChartSample1(monthlyUser);
            initChartSample2(monthlyChat);         
        }

    };

}();