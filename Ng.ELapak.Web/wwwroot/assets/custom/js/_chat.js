﻿$(document).ready(function () {
    $(".chat_on").click(function () {
        $(".Layout").toggle();
        $(".chat_on").hide(300);
    });

    $(".chat_close_icon").click(function () {
        $(".Layout").hide();
        $(".chat_on").show(300);
    });

});

$('#btn-chat').on('click', function () {
    var message = $('#btn-input').val();
    sendMessage(message, "test");
});
$('#btn-input').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        var message = $('#btn-input').val();
        //sendMessage(message, "test");
        addMessageContent(message, "send");
        sendMessage(message, "test");
        //addMessageContent("Apa Kabar", "receive");
    }
});
function sendMessage(message, userId) {
    var baseUrl = document.baseURI;
    userId = randomId();
    $.ajax({
        url: baseUrl + 'api/Message/postmessage',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        data: JSON.stringify({
            userId: userId,
            message: message,
        }),
        success: function (resp) {
            //console.log(resp);
            if (resp.alert.code == 200) {
                var data = resp.data;
                $.each(data, function (key, value) {
                    addMessageContent(value.content, "receive");
                });
                //console.log(data);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('internal error');
        }
    });
}
function addMessageContent(message, type) {
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    if (type == "send") {
        var send = '';
        send += '<li class="left clearfix">';
        send += '<span class="chat-img pull-left" > <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /></span >';
        send += '<div class="chat-body clearfix">';
        send += '<div class="header">';
        send += '<small class="pull-right text-muted"><span class="glyphicon glyphicon-time">' + time + '</span></small>';
        send += '</div>';
        send += '<p>' + message + '</p>';
        send += '</div>';
        send += '</li >';
        //console.log(send);
        $(".chat").append(send);
    }
    else {
        var send = '';
        send += '<li class="right clearfix">';
        send += '<span class="chat-img pull-right"> <img src="http://placehold.it/50/ffc107/fff&text=BOT" alt="User Avatar" class="img-circle" /></span >';
        send += '<div class="chat-body clearfix">';
        send += '<div class="header">';
        send += '<small class=" text-muted"><span class="glyphicon glyphicon-time">' + time + '</span></small>';
        send += '</div>';
        send += '<p>' + message + '</p>';
        send += '</div>';
        send += '</li >';
        //console.log(send);
        $(".chat").append(send);
    }
    var scrollHeight = $(".chat")[0].scrollHeight;
    console.log(scrollHeight);
    $(".chat").scrollTop(scrollHeight);
    //$(".chat").slimScroll({ scrollTo: scrollHeight + 'px' });
    //$(".chat").animate({ scrollTop: scrollHeight }, "fast");
    //$(".chat").scrollTop = scrollHeight;
   
    $('#btn-input').val('');
};
function randomId() {
    var UserId = localStorage.getItem("chatBotUserId");
    if (UserId == null) {
        const uint32 = window.crypto.getRandomValues(new Uint32Array(1))[0];
        UserId = uint32.toString(16);
        localStorage.setItem("chatBotUserId", UserId);
    }
    return UserId;
}