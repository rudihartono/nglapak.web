﻿jQuery(document).ready(function () {
    var oTable = $('#table-list').DataTable({
        "bServerSide": true,
        "bProcessing": true,
        'autoWidth': false,
        'order': [[0, 'desc']],
        'iDisplayLength': 10,
        'lengthMenu': [10, 25, 50, 75, 100],
        'oLanguage': {
            'sSearch': 'Search'
        },
        "ajax": {
            'url': urlDataTable,
            'data': function (d) {
                return {
                    target: 'chatbotuser',
                    search: d.search.value,
                    length: d.length,
                    start: d.start,
                    orderCol: d.order[0].column,
                    orderType: d.order[0].dir
                };
            }
        },
        "aoColumns": [
            {
                'mData': function (s) {
                    return '';
                }, 'bSortable': false, sClass: 'text-center'
            },
            {
                'mData': function (s) {
                    return '<a class="btn btn-padd-small blue" href="ChatbotUser/Detail/' + s.id + '">Lihat Detail</a>';
                }, 'bSortable': false, sClass: 'text-center'
            },
            { 'mData': 'channelUserId' },
            { 'mData': 'channelType' },
            { 'mData': 'fullName' },
            { 'mData': 'email' },
            { 'mData': 'phone' },
            { 'mData': 'address' },
            { 'mData': 'rating' }
        ],
        rowCallback: function (row, data, index) {
            var dtInfo = oTable.page.info();

            var arrTd = $(row).children();
            $(arrTd[0]).html(dtInfo.start + index + 1);

            return row;
        },
        fnDrawCallback: function (oSettings) {
            //$('.tooltips').tooltip();
        }
    });
});