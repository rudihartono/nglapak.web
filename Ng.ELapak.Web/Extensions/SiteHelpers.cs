﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Extensions
{
    public class SiteHelpers
    {

    }

    public class SiteMenu
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Icon { get; set; }

        public string Url { get; set; }

        public int[] UserTypes { get; set; }

        public List<SiteMenu> ListSubmenu { get; set; }
    }
}
