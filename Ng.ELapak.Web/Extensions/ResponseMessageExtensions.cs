﻿using Ng.ELapak.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Extensions
{
    public static class ResponseMessageExtensions
    {
        public static ResponseMessage SetData(this ResponseMessage responseMessage, object data)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            responseMessage.SetDataInternal(data: data);
            return responseMessage;
        }

        public static ResponseMessage SetOk(this ResponseMessage responseMessage,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetOK(message: message, innerMessage: innerMessage);
            return responseMessage;
        }

        public static ResponseMessage SetUnauthorized(this ResponseMessage responseMessage,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetUnauthorized(message: message, innerMessage: innerMessage);
            return responseMessage;
        }

        public static ResponseMessage SetNotFound(this ResponseMessage responseMessage,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetNotFound(message: message, innerMessage: innerMessage);
            return responseMessage;
        }

        public static ResponseMessage SetBadRequest(this ResponseMessage responseMessage,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetBadRequest(message: message, innerMessage: innerMessage);
            return responseMessage;
        }

        public static ResponseMessage SetForbidden(this ResponseMessage responseMessage,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetForbidden(message: message, innerMessage: innerMessage);
            return responseMessage;
        }

        public static ResponseMessage SetInternalServerError(this ResponseMessage responseMessage,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetInternalServerError(message: message, innerMessage: innerMessage);
            return responseMessage;
        }

        public static ResponseMessage SetCustomCode(this ResponseMessage responseMessage,
            int code,
            string message,
            string innerMessage = null)
        {
            if (responseMessage == null)
            {
                throw new ArgumentNullException(nameof(responseMessage));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("message", nameof(message));
            }

            responseMessage.Alerts.SetCustomCode(code: code, message: message, innerMessage: innerMessage);
            return responseMessage;
        }
    }
}
