﻿using Ng.ELapak.Web.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;

namespace Ng.ELapak.Web.Domain.Db
{
    public class AppDbContenxt: DbContext
    {
        public AppDbContenxt(DbContextOptions<AppDbContenxt> options):
            base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<ChatbotUser> ChatbotUser { get; set; }
        public DbSet<TotalChatByDate> TotalChatByDate { get; set; }
        public DbSet<ChatHistoryFAQ> ChatHistoryFAQ { get; set; }
        public DbSet<Ng.ELapak.Web.Areas.Admin.Models.UserViewModel> UserViewModel { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public DbSet<ProductPurchaseOrder> ProductPurchaseOrder { get; set; }
        public DbSet<UserProductDiscount> UserProductDiscount { get; set; }
        public DbSet<MessageSession> MessageSession { get; set; }
    }
}
