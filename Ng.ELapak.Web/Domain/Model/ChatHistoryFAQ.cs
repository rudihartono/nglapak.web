﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    [Table("ChatHistoryFAQ")]
    public class ChatHistoryFAQ : BaseModel
    {
        public DateTime Date { get; set; }
        public string MessageContent { get; set; }
        public string MessageResponse { get; set; }
        public string SessionId { get; set; }
    }
}
