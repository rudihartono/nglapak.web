﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    [Table("ChatbotUser")]
    public class ChatbotUser : BaseModel
    {
        [Required]
        [StringLength(50)]
        public string ChannelUserId { get; set; }
        [StringLength(50)]
        public string ChannelType { get; set; }
        [StringLength(100)]
        public string FullName { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(50)]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Address { get; set; }
        public DateTime LastActivity { get; set; }
        public int Rating { get; set; }
    }
}
