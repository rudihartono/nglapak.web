﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    [Table("MessageSession")]
    public class MessageSession : BaseModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string SessionContent { get; set; }
    }
}
