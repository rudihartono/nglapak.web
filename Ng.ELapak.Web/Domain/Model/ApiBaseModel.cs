﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    public class ApiBaseModel
    {
        [Required]
        [MaxLength(50)]
        public string ChannelUserId { get; set; }

        [MaxLength(50)]
        public string ChannelType { get; set; }
    }

    public class UserChatbotApiData
    {
        public string ChannelUserId { get; set; }
        public string ChannelType { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }

    public class ChatbotUserApiModel : ApiBaseModel
    {
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [MaxLength(100)]
        public string Phone { get; set; }

        [Required]
        [MaxLength(100)]
        public string Address { get; set; }
    }

    public class RatingUserApiModel : ApiBaseModel
    {
        [Required]
        public string Rating { get; set; }
    }

    public class HandOverApiModel : ApiBaseModel
    {
        [Required]
        public string RoomId { get; set; }
    }
}
