﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    public enum UserType
    {
        ADMIN,
        CUSTOMER
    }

    [Table("User")]
    public class User : BaseModel
    {
        public string Name { get; set; }
        public string Role { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public UserType Type { get; set; }
       
    }
}
