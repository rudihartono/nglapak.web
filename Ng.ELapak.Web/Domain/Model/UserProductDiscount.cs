﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    [Table("UserProductDiscount")]
    public class UserProductDiscount: BaseModel
    {
        public string ProductId { get; set; }
        public string UserId { get; set; }
        public double Discount { get; set; }
        public decimal Price { get; set; }
        public decimal PriceAfterDiscount { get; set; }
    }
}
