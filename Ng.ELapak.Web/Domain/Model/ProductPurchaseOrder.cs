﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    [Table("ProductPurchaseOrder")]
    public class ProductPurchaseOrder:BaseModel
    {
        public string PurchaseOrderId { get; set; }
        public string ProductId { get; set; }
        public string UserId { get; set; }
        public double Discount { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
