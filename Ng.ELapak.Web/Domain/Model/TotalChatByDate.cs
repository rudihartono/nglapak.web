﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Model
{
    [Table("TotalChatByDate")]
    public class TotalChatByDate : BaseModel
    {
        public DateTime Date { get; set; }
        public int Total { get; set; }
        public string SessionId { get; set; }
    }
}
