﻿using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Model;
using Ng.ELapak.Web.Extensions;
using Ng.ELapak.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Services
{
    public class ChatbotUserService : IChatbotUserService
    {
        private readonly ResponseMessage _responseMessage;
        private readonly AppDbContenxt _AppDbContext;
        private readonly ILogger _logger;
        public ChatbotUserService(AppDbContenxt appDbContenxt, ILogger<ChatbotUserService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
            _responseMessage = new ResponseMessage();
        }

        public async Task<ResponseMessage> CheckStatus(ApiBaseModel model)
        {
            ChatbotUser chatbotUser = await _AppDbContext.ChatbotUser
                .Where(x => x.ChannelUserId.Equals(model.ChannelUserId) && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();
            if (chatbotUser == null)
            {
               return await Task.FromResult(_responseMessage.SetNotFound(message:"User not found"));
            }
            UserChatbotApiData ret = new UserChatbotApiData
            {
                ChannelUserId = chatbotUser.ChannelUserId,
                FullName = chatbotUser.FullName,
                Address = chatbotUser.Address,
                ChannelType = chatbotUser.ChannelType,
                Email = chatbotUser.Email,
                Phone = chatbotUser.Phone
            };
            return await Task.FromResult(_responseMessage.SetData(ret)); ;
        }

        public async Task<ResponseMessage> SaveChatbotUser(ChatbotUserApiModel model)
        {
            bool isUpdate = false;
            ChatbotUser chatbotUser = await _AppDbContext.ChatbotUser
                .Where(x => x.ChannelUserId.Equals(model.ChannelUserId) && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();
            if (chatbotUser == null)
            {
                chatbotUser = new ChatbotUser()
                {
                    ChannelUserId = model.ChannelUserId,
                    FullName = model.FullName,
                    Phone = model.Phone,
                    Address = model.Address,
                    Email = model.Email,
                    CreateDt = DateTime.UtcNow,
                    ChannelType = model.ChannelType
                };
            }
            else
            {
                isUpdate = true;

                if (!string.IsNullOrEmpty(model.ChannelType))
                {
                    chatbotUser.ChannelType = model.ChannelType;
                }
                chatbotUser.FullName = model.FullName;
                chatbotUser.Phone = model.Phone;
                chatbotUser.Email = model.Email;
                chatbotUser.Address = model.Address;
                chatbotUser.UpdatedDt = DateTime.UtcNow;
            }

            try
            {
                if (isUpdate)
                    _AppDbContext.ChatbotUser.Update(chatbotUser);
                else
                    _AppDbContext.ChatbotUser.Add(chatbotUser);
                await _AppDbContext.SaveChangesAsync();
                return await Task.FromResult(_responseMessage.SetOk(message: "User save success"));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(_responseMessage.SetInternalServerError(message: ex.Message,innerMessage: ex.InnerException.Message));
            }
        }

        public async Task<ResponseMessage> SaveRating(RatingUserApiModel model)
        {
            ChatbotUser chatbotUser = await _AppDbContext.ChatbotUser
                .Where(x => x.ChannelUserId.Equals(model.ChannelUserId) && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();
            if (chatbotUser == null)
            {
                return await Task.FromResult(_responseMessage.SetNotFound(message: "User not found"));
            }

            if(int.Parse(model.Rating) > 10)
            {
                return await Task.FromResult(
                _responseMessage.SetCustomCode(408,"rating must 1 - 10"));
            }
            try
            {
                chatbotUser.Rating = int.Parse(model.Rating);
                _AppDbContext.ChatbotUser.Update(chatbotUser);
                await _AppDbContext.SaveChangesAsync();
                return await Task.FromResult(_responseMessage.SetOk(message: "rating save success"));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(_responseMessage.SetInternalServerError(message: ex.Message, innerMessage: ex.InnerException.Message));
            }
        }

        public async Task<ResponseMessage> HandOver(HandOverApiModel model)
        {
            ChatbotUser chatbotUser = await _AppDbContext.ChatbotUser
                .Where(x => x.ChannelUserId.Equals(model.ChannelUserId) && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();
            if (chatbotUser == null)
            {
                return await Task.FromResult(_responseMessage.SetNotFound(message: "User not found"));
            }

            string sdk_Key = "d8356560a6a146a6dd627d96bdc0663e";
            string app_Id = "retlo-gw4ja5sc3dumacp";
            string url = "https://qismo.qiscus.com/" + app_Id + "/bot/" + model.RoomId + "/hand_over";
            try
            {
                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(url),
                        Headers =
                            {
                                { HttpRequestHeader.Authorization.ToString(), sdk_Key }
                            }
                    };
                    var response = await client.SendAsync(request);
                    var content = await response.Content.ReadAsStringAsync();
                    _logger.LogInformation(content);
                }
                return await Task.FromResult(_responseMessage.SetOk(message: "Anda Segera di hubungkan ke CS"));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                //return await Task.FromResult(_responseMessage.SetInternalServerError(message: ex.Message, innerMessage: ex.InnerException.Message));
                return await Task.FromResult(_responseMessage.SetOk(message: "Mohon maaf terjadi kesalahan di system kami"));
            }
        }
    }

    public interface IChatbotUserService
    {
        /// <summary>
        /// Check status memanggil API ke thirdparty untuk mengetahui status dari user
        /// </summary>
        /// <param name="model">isi dengan userchannel id, dan channel type</param>
        /// <returns></returns>
        Task<ResponseMessage> CheckStatus(ApiBaseModel model);
        Task<ResponseMessage> SaveChatbotUser(ChatbotUserApiModel model);
        Task<ResponseMessage> SaveRating(RatingUserApiModel model);
        Task<ResponseMessage> HandOver(HandOverApiModel model);
    }
}
