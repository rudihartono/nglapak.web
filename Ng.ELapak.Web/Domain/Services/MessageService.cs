﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Model;
using Ng.ELapak.Web.Extensions;
using Ng.ELapak.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Services
{
    public class MessageService : IMessageService
    {
        private readonly ResponseMessage _responseMessage;
        private readonly AppDbContenxt _AppDbContext;
        private readonly ILogger _logger;
        public MessageService (AppDbContenxt appDbContenxt, ILogger<MessageService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
            _responseMessage = new ResponseMessage();
        }

        public async Task<ResponseMessage> PostMessage(PostMessage model)
        {
            string token = "Bearer 8157f594-7630-472c-a39c-94595176a133";
            string dSession = File.ReadAllText("session.json");
            string url = "https://api.kata.ai/projects/37266398-e222-4055-8e95-59d063ac1f64/bot/converse";
            bool isUpdate = false;
            try
            {
                //Session session = new Session();
                object session = new object();
                MessageSession mSession = _AppDbContext.MessageSession.Where(x => x.UserId.Equals(model.UserId)).FirstOrDefault();
                if(mSession == null)
                {
                    var userId = JsonConvert.DeserializeObject<Session>(dSession);
                    userId.id = model.UserId;
                    session = userId;
                }
                else
                {
                    isUpdate = true;
                    session = JsonConvert.DeserializeObject<object>(mSession.SessionContent);
                }

                Message message = new Message();
                message.type = "text";
                message.content = model.Message;
                MessageSendModel postModel = new MessageSendModel()
                {
                    environmentName = "Development",
                    message = message,
                    session = session
                };
                _logger.LogInformation(JsonConvert.SerializeObject(postModel));
                var post = JsonConvert.SerializeObject(postModel);
                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(url),
                        Headers =
                            {
                                { HttpRequestHeader.Authorization.ToString(), token }
                            },
                        Content = new StringContent(post, Encoding.UTF8, "application/json"),
                    };
                    var response = await client.SendAsync(request);
                    MessageResponseModel messageResponseModel = new MessageResponseModel();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        JObject obj = JObject.Parse(content);
                        string sessionContent = "";
                        foreach(var ses in obj)
                        {
                            if(ses.Key == "session")
                            {
                                sessionContent = JsonConvert.SerializeObject(ses.Value);
                            }
                        }
                        messageResponseModel = JsonConvert.DeserializeObject<MessageResponseModel>(content);
                        if (isUpdate)
                        {
                            mSession.SessionContent = sessionContent;
                            mSession.UpdatedDt = DateTime.Now;
                            _AppDbContext.MessageSession.Update(mSession);
                        }
                        else
                        {
                            mSession = new MessageSession()
                            {
                                UserId = model.UserId,
                                SessionContent = sessionContent,
                                CreateDt = DateTime.Now
                            };
                            _AppDbContext.MessageSession.Add(mSession);
                        }
                        await _AppDbContext.SaveChangesAsync();
                    }
                    return await Task.FromResult(_responseMessage.SetData(messageResponseModel.responses));
                }
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return await Task.FromResult(_responseMessage.SetBadRequest(message: "Mohon maaf terjadi kesalahan di system kami"));
            }
        }

    }

    public interface IMessageService
    {
        Task<ResponseMessage> PostMessage(PostMessage model);
    }
}
