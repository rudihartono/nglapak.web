﻿using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Model;
using Ng.ELapak.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Services
{
    public class CronChatHistoryService : ICronChatHistoryService
    {
        private readonly AppDbContenxt _AppDbContext;
        private readonly ILogger _logger;
        private readonly IOptions<KataConfigurationSetting> _kataConfigurationSetting;
        public CronChatHistoryService(AppDbContenxt appDbContenxt, ILogger<CronChatHistoryService> logger, IOptions<KataConfigurationSetting> setting)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
            _kataConfigurationSetting = setting;
        }

        public async Task UpdateChatHistory()
        {
            DateTime dateChat = DateTime.Now.AddDays(-1);
            await UpdateChatHistoryByPeriode(dateChat);
        }

        private async Task<string> GetTokenKata()
        {
            string url = _kataConfigurationSetting.Value.KataUrl;
            string token = "";
            try
            {
                object model = new
                {
                    username = _kataConfigurationSetting.Value.KataUser,
                    password = _kataConfigurationSetting.Value.KataPass
                };
                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"),
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(url),
                    };
                    var responseSession = await client.SendAsync(request);
                    var contentSession = await responseSession.Content.ReadAsStringAsync();
                    KataReturnModel returnModel = JsonConvert.DeserializeObject<KataReturnModel>(contentSession);
                    token = await GetTokenKataTeamId(returnModel.id);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }
            return token;
        }

        private async Task<string> GetTokenKataTeamId(string tokenUser)
        {
            string url = "https://api.kata.ai/tokens";
            string token = "";
            try
            {
                object model = new
                {
                    type = "team",
                    teamId = _kataConfigurationSetting.Value.TeamID
                };
                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"),
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(url),
                        Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString() , string.Format("Bearer {0}", tokenUser) }
                        }
                    };
                    var responseSession = await client.SendAsync(request);
                    var contentSession = await responseSession.Content.ReadAsStringAsync();
                    KataReturnModel returnModel = JsonConvert.DeserializeObject<KataReturnModel>(contentSession);
                    token = returnModel.id;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }
            return token;
        }

        public async Task UpdateChatHistoryByPeriode(DateTime date)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            
            string projectId = _kataConfigurationSetting.Value.ProjectID;
            string envId = _kataConfigurationSetting.Value.EnvironmentID;
            string stringDateTimeStart = date.ToString("yyyy-MM-dd 00:00:00");
            string stringDateTimeEnd = date.ToString("yyyy-MM-dd 23:59:00");
            string startTimestamp = DateTimeOffset.Parse(stringDateTimeStart).ToUnixTimeMilliseconds().ToString();
            string endTimestamp = DateTimeOffset.Parse(stringDateTimeEnd).ToUnixTimeMilliseconds().ToString();
            string token = await GetTokenKata();

            string urlGetSession = "https://api.kata.ai/analytics/" + projectId + "/" + envId + "/list_transcript?startTimestamp=" + startTimestamp + "&endTimestamp=" + endTimestamp;
            try
            {
                List<SessionId> listSession = new List<SessionId>();
                
                using (var client = new HttpClient(clientHandler))
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(urlGetSession),
                        Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString() , string.Format("Bearer {0}", token) }
                        }
                    };
                    var responseSession = await client.SendAsync(request);
                    var contentSession = await responseSession.Content.ReadAsStringAsync();
                    listSession = JsonConvert.DeserializeObject<List<SessionId>>(contentSession);
                }
                if(listSession != null)
                {
                    await GetConversationChat(listSession, startTimestamp, endTimestamp, token, date);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }
        }

        private async Task GetConversationChat(List<SessionId> listSession, string startTimestamp, string endTimestamp, string token, DateTime date)
        {
            string projectId = _kataConfigurationSetting.Value.ProjectID;
            string envId = _kataConfigurationSetting.Value.EnvironmentID;
            List<ConversationChat> listConversationChat = new List<ConversationChat>();
            try
            {
                foreach (var session in listSession)
                {
                    string url = "https://api.kata.ai/analytics/" + projectId + "/" + envId + "/" + session.sessionid + "/transcript?startTimestamp=" + startTimestamp + "&endTimestamp=" + endTimestamp;
                    using (var client = new HttpClient())
                    {
                        HttpRequestMessage request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Get,
                            RequestUri = new Uri(url),
                            Headers =
                            {
                                { HttpRequestHeader.Authorization.ToString() , string.Format("Bearer {0}", token) }
                            }
                            };
                        var response = await client.SendAsync(request);
                        var content = await response.Content.ReadAsStringAsync();
                        listConversationChat = JsonConvert.DeserializeObject<List<ConversationChat>>(content);
                        if(listConversationChat != null)
                        {
                            await ProcessHistoryChat(listConversationChat, session.sessionid, date);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }
        }

        private async Task ProcessHistoryChat(List<ConversationChat> listConversationChat,string sessionId, DateTime date)
        {
            await UpdateTotalChat(listConversationChat, sessionId, date);
            foreach (var chat in listConversationChat)
            {
                var message = JsonConvert.DeserializeObject<List<messages>>(chat.messages);
                var response = JsonConvert.DeserializeObject<List<responses>>(chat.responses);
              
                foreach (var m in message.Where(x => x.intent.Equals("faqIntent")))
                {
                    string id = m.id;
                    DateTime dateChat = UnixTimeToDateTime(chat.created_at);
                    string messageContent = m.content;
                    string messageResponse = response.Where(x => x.refId.Equals(id)).FirstOrDefault().content;
                    await UpdateHistoryChatFAQ(sessionId, messageContent, messageResponse, dateChat);
                }
            }
        }

        private async Task UpdateTotalChat(List<ConversationChat> listConversationChat, string sessionId, DateTime date)
        {
            try
            {
                TotalChatByDate totalChatByDate = await _AppDbContext.TotalChatByDate
              .Where(x => x.Date.ToString("yyyy-MM-dd").Equals(date.ToString("yyyy-MM-dd")) && x.SessionId.Equals(sessionId)).FirstOrDefaultAsync();
                if (totalChatByDate != null)
                {
                    totalChatByDate.Total = listConversationChat.Count();
                    totalChatByDate.UpdatedDt = DateTime.Now;
                    _AppDbContext.TotalChatByDate.Update(totalChatByDate);
                    await _AppDbContext.SaveChangesAsync();
                }
                else
                {
                    TotalChatByDate add = new TotalChatByDate()
                    {
                        SessionId = sessionId,
                        Total = listConversationChat.Count(),
                        Date = date,
                        CreateDt = DateTime.Now
                    };
                    _AppDbContext.TotalChatByDate.Add(add);
                    await _AppDbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }
        }

        private async Task UpdateHistoryChatFAQ(string sessionId, string messageContent, string messageResponse, DateTime date)
        {
            try
            {
                ChatHistoryFAQ chatHistoryFAQ = await _AppDbContext.ChatHistoryFAQ
              .Where(x => x.Date.Equals(date) && x.SessionId.Equals(sessionId) && x.MessageContent.Equals(messageContent)).FirstOrDefaultAsync();
                if (chatHistoryFAQ != null)
                {
                    chatHistoryFAQ.MessageContent = messageContent;
                    chatHistoryFAQ.MessageResponse = messageResponse;
                    chatHistoryFAQ.UpdatedDt = DateTime.Now;
                    _AppDbContext.ChatHistoryFAQ.Update(chatHistoryFAQ);
                    await _AppDbContext.SaveChangesAsync();
                }
                else
                {
                    ChatHistoryFAQ add = new ChatHistoryFAQ()
                    {
                        SessionId = sessionId,
                        MessageContent = messageContent,
                        MessageResponse = messageResponse,
                        Date = date,
                        CreateDt = DateTime.Now
                    };
                    _AppDbContext.ChatHistoryFAQ.Add(add);
                    await _AppDbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.Message);
            }
        }

        private static DateTime UnixTimeToDateTime(long unixtime)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixtime).ToLocalTime();
            return dtDateTime;
        }
    }

    public interface ICronChatHistoryService
    {
        Task UpdateChatHistory();
        Task UpdateChatHistoryByPeriode(DateTime date);
    }
}
