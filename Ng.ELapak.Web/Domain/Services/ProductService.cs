﻿using Microsoft.Extensions.Logging;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Model;
using Ng.ELapak.Web.Extensions;
using Ng.ELapak.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ng.ELapak.Web.Domain.Services
{
    public class ProductService : IProductService
    {
        private readonly ResponseMessage _responseMessage;
        private readonly AppDbContenxt _AppDbContext;
        private readonly ILogger _logger;
        public ProductService(AppDbContenxt appDbContenxt, ILogger<ProductService> logger)
        {
            _AppDbContext = appDbContenxt;
            _logger = logger;
            _responseMessage = new ResponseMessage();
        }

        public async Task<ResponseMessage> Search(ProductSearchApi model)
        {
            try
            {
                List<Product> listProduct = _AppDbContext.Product.Where(x => (x.Description.Contains(model.Keyword) || x.Name.Contains(model.Keyword)) && x.IsDeleted == false).ToList();
                int total = 0;
                if(listProduct == null)
                {
                    return await Task.FromResult(_responseMessage.SetBadRequest(message: "Product tidak tersedia"));
                }
                else
                {
                    int offset = (model.Page - 1) * model.Limit;
                    total = listProduct.Count;
                    List<ProductViewModel> list = new List<ProductViewModel>();
                    foreach (var product in listProduct.Skip(offset).Take(model.Limit))
                    {
                        ProductViewModel productViewModel = new ProductViewModel()
                        {
                            ProductId = product.Id,
                            Price = product.Price,
                            ProductCode = product.Code,
                            Description = product.Description,
                            ImageUrl = product.ImageUrl,
                            Name = product.Name
                        };
                        list.Add(productViewModel);
                    }

                    object res = new
                    {
                        DataResult = list,
                        TotalData = total
                    };

                    return await Task.FromResult(_responseMessage.SetData(res));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return await Task.FromResult(_responseMessage.SetBadRequest(message: "Mohon maaf terjadi kesalahan di system kami"));
            }
        }

        public async Task<ResponseMessage> Detail(ProductApi model)
        {
            try
            {
                Product product = _AppDbContext.Product.Where(x => x.Code.Equals(model.ProductCode) && x.IsDeleted == false).FirstOrDefault();
                if (product == null)
                {
                    return await Task.FromResult(_responseMessage.SetOk(message: "Product tidak tersedia"));
                }
                else
                {
                    ProductViewModel productViewModel = new ProductViewModel()
                    {
                        ProductId = product.Id,
                        Price = product.Price,
                        ProductCode = product.Code,
                        Description = product.Description,
                        ImageUrl = product.ImageUrl,
                        Name = product.Name
                    };
                    return await Task.FromResult(_responseMessage.SetData(productViewModel));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return await Task.FromResult(_responseMessage.SetBadRequest(message: "Mohon maaf terjadi kesalahan di system kami"));
            }
        }

        public async Task<ResponseMessage> Bid(BidProductApi model)
        {
            try
            {
                Product product = _AppDbContext.Product.Where(x => x.Code.Equals(model.ProductCode) && x.IsDeleted == false).FirstOrDefault();
                if (product == null)
                {
                    return await Task.FromResult(_responseMessage.SetBadRequest(message: "Not Ok"));
                }
                else
                {
                    double bid = model.BidPrice;
                    double price = (double)product.Price - (double)product.Price*product.MaxDiscount;
                    if(bid >= price)
                    {
                        return await Task.FromResult(_responseMessage.SetOk(message: "Ok"));
                    }
                    else
                    {
                        return await Task.FromResult(_responseMessage.SetOk(message: "Not Ok"));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return await Task.FromResult(_responseMessage.SetBadRequest(message: "Mohon maaf terjadi kesalahan di system kami"));
            }
        }

        public async Task<ResponseMessage> Purchase(PurchaseApi model)
        {
            try
            {
                Product product = _AppDbContext.Product.Where(x => x.Code.Equals(model.ProductCode) && x.IsDeleted == false).FirstOrDefault();
                if (product == null)
                {
                    return await Task.FromResult(_responseMessage.SetBadRequest(message: "Not Ok"));
                }
                else
                {
                    ChatbotUser chatbotUser = _AppDbContext.ChatbotUser.Where(x => x.ChannelUserId.Equals(model.UserId)).FirstOrDefault();
                    if(chatbotUser != null)
                    {
                        PurchaseOrder purchaseOrder = new PurchaseOrder()
                        {
                            ProductId = product.Id,
                            Quantity = model.Qty,
                            TotalPrice = model.Qty * (decimal)model.BidPrice,
                            UserId = chatbotUser.Id,
                            CreateDt = DateTime.Now
                        };

                        ProductPurchaseOrder productPurchaseOrder = new ProductPurchaseOrder()
                        {
                            ProductId = product.Id,
                            ProductPrice = product.Price,
                            PurchaseOrderId = purchaseOrder.Id,
                            Discount = (double)(product.Price - (decimal)model.BidPrice),
                            CreateDt = DateTime.Now,
                            UserId = chatbotUser.Id
                        };

                        _AppDbContext.ProductPurchaseOrder.Add(productPurchaseOrder);
                        _AppDbContext.PurchaseOrder.Add(purchaseOrder);
                        await _AppDbContext.SaveChangesAsync();
                        return await Task.FromResult(_responseMessage.SetData(purchaseOrder));
                    }
                    else
                    {
                        return await Task.FromResult(_responseMessage.SetBadRequest(message: "User Tidak Tersedia"));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return await Task.FromResult(_responseMessage.SetBadRequest(message: "Mohon maaf terjadi kesalahan di system kami"));
            }
        }
    }

    public interface IProductService
    {
        Task<ResponseMessage> Detail(ProductApi model);
        Task<ResponseMessage> Search(ProductSearchApi model);
        Task<ResponseMessage> Bid(BidProductApi model);
        Task<ResponseMessage> Purchase(PurchaseApi model);
    }
}
