﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ng.ELapak.Web.Migrations
{
    public partial class AddTableTotalChatByDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TotalChatByDate",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreateDt = table.Column<DateTime>(nullable: false),
                    UpdatedDt = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Total = table.Column<int>(nullable: false),
                    SessionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TotalChatByDate", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TotalChatByDate");
        }
    }
}
