﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ng.ELapak.Web.Migrations
{
    public partial class AddTableHistoryFAQ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChatHistoryFAQ",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreateDt = table.Column<DateTime>(nullable: false),
                    UpdatedDt = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    MessageContent = table.Column<string>(nullable: true),
                    MessageResponse = table.Column<string>(nullable: true),
                    SessionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatHistoryFAQ", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChatHistoryFAQ");
        }
    }
}
