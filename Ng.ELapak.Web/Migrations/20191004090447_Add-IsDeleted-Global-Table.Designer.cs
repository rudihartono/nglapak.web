﻿// <auto-generated />
using System;
using Ng.ELapak.Web.Domain.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Ng.ELapak.Web.Migrations
{
    [DbContext(typeof(AppDbContenxt))]
    [Migration("20191004090447_Add-IsDeleted-Global-Table")]
    partial class AddIsDeletedGlobalTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Ng.ELapak.Web.Domain.Model.ChatbotUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(50);

                    b.Property<string>("ChannelType")
                        .HasMaxLength(50);

                    b.Property<string>("ChannelUserId")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreateDt");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FullName")
                        .HasMaxLength(100);

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime>("LastActivity");

                    b.Property<string>("Phone")
                        .HasMaxLength(50);

                    b.Property<DateTime>("UpdatedDt");

                    b.HasKey("Id");

                    b.ToTable("ChatbotUser");
                });

            modelBuilder.Entity("Ng.ELapak.Web.Domain.Model.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDt");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<string>("Role");

                    b.Property<DateTime>("UpdatedDt");

                    b.Property<string>("Username")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("User");
                });
#pragma warning restore 612, 618
        }
    }
}
