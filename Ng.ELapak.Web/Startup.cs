﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ng.ELapak.Web.Areas.Admin.Models;
using Ng.ELapak.Web.Areas.Admin.Services;
using Ng.ELapak.Web.Domain.Db;
using Ng.ELapak.Web.Domain.Services;
using Ng.ELapak.Web.Filters;
using Ng.ELapak.Web.Models;
using Hangfire;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Examples;
using Swashbuckle.AspNetCore.Swagger;

namespace Ng.ELapak.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContenxt>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddAuthentication(options=>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
                .AddCookie(
                authenticationScheme: CookieAuthenticationDefaults.AuthenticationScheme,
                options =>
                {
                    options.LoginPath = "/admin/authentication/index";
                    options.LogoutPath = "/admin/authentication/logout";
                });

            services.AddAuthorization();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Application API", Version = "v1" });
                //c.OperationFilter<SwashbuckleConfig>();
            });
            //KataConfigurationSetting
            services.AddSingleton<AuthenticationService>();
            services.Configure<AuthenticationSetting>(Configuration.GetSection("AuthenticationSetting"));
            services.Configure<KataConfigurationSetting>(Configuration.GetSection("KataConfigurationSetting"));
            services.AddMvc(options=>
            {
                options.EnableEndpointRouting = false;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")));
            services.AddHangfireServer();

            services.AddScoped<ICronChatHistoryService, CronChatHistoryService>();
            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IRecurringJobManager recurringJobManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ng.ELapak API V1");
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[]
                {
                    new HangFireAuthorizationFilter(),
                },
            });

            //app.UseHangfireServer();
            //RecurringJob.AddOrUpdate<ICronChatHistoryService>(
            //_cronJob => _cronJob.UpdateChatHistory(), Cron.Daily(1));

            app.UseMvc(configureRoutes =>
            {
                configureRoutes.MapAreaRoute(
                    name: "ecommercearea",
                    areaName: "ecommerce",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                configureRoutes.MapAreaRoute(
                    name: "areas",
                    areaName: "Admin",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
            });


        }
    }
}
